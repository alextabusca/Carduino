// Chapter 5 - Motor Control
// Controlling Speed
// By Cornel Amariei for Packt Publishing

// Include the Servo library
#include <Servo.h>
#include <SoftwareSerial.h>
#include <NewPing.h>

SoftwareSerial BTSerial(10, 11); // RX | TX

#define TRIG_PIN A4
#define ECHO_PIN A5
#define MAX_DISTANCE 200
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);

//String for receiving data from BT Phone
String readString;

// Create a servo object
Servo myServo;
// Declare the Servo pin
int servoPin = 2;

//Connections for ultrasonic sensor - for calculateDisstance funtion
//const int trigPin = 6;
//const int echoPin = 12;

long duration;
int distance;
int otherDistance;

//Right motors
const int motorPin1 = 4;
const int motorPin2 = 5;

//Left motors
const int motorPin3 = 7;
const int motorPin4 = 8;

//Other variables
boolean hasLooked = false;
int n;
int command;

const long baudRate = 38400;
char c = ' ';
boolean NL = true;


void setup()
{
  // We need to attach the servo to the used pin number
  myServo.attach(servoPin);
  myServo.write(125);
  delay(1000);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);

  //Setup for ultrasonic sensor pins - for calculateDistance function
  //pinMode(trigPin, OUTPUT);
  //pinMode(echoPin, INPUT);

  //Serial baud rate
  Serial.begin(9600);

  //Bluetooth serial baud rate
  BTSerial.begin(9600);
}

void loop()
{

  while (BTSerial.available())
  {
    readString += char(BTSerial.read());
    //char c = Serial.read();  //gets one byte from serial buffer
    //readString += c; //makes the string readString
    delay(2);  //slow looping to allow buffer to fill with next character
  }

  if (readString.length() > 0)
  {
    //Serial.println(readString);  //so you can see the captured string
    n = readString.toInt();  //convert readString into a number
  }

  if (n > 0)
    command = n;

  // auto select appropriate value, copied from someone elses code.
  if (command == 1)
  {
    otherDistance = readPing();
    if (otherDistance <= 30)
    {
      stopMoving();
      delay(100);
      //moveBackward();
      //delay(300);
      //stopMoving();
    }
    else
    {
      moveForward();
      delay(100);
    }
  }

  if (command == 2)
  {
    moveBackward();
    delay(100);
  }

  if (command == 3)
  {
    turnRight();
    delay(100);
  }

  if (command == 4)
  {
    turnLeft();
    delay(100);
  }

  if (command == 5)
  {
    stopMoving();
    delay(100);
  }

  if (command == 6)
  {
    selfDriveMode();
    delay(100);
  }

  readString = ""; //empty for next input
}

//void calculateDistance()
//{
//  //Clears the trigPin
//  digitalWrite(trigPin, LOW);
//  delayMicroseconds(2);
//  //Sets the trigPin on HIGH state for 10 microseconds
//  digitalWrite(trigPin, HIGH);
//  delayMicroseconds(10);
//  digitalWrite(trigPin, LOW);
//  //Reads the echoPin, returns the sound wave travel time in microseconds
//  duration = pulseIn(echoPin, HIGH);
//  //Calculate the distance
//  distance = duration * 0.034 / 2;
//}

void moveForward()
{
  //RightSide move forward
  analogWrite(motorPin1, 180);
  analogWrite(motorPin2, 0);

  //LeftSide move forward
  analogWrite(motorPin3, 0);
  analogWrite(motorPin4, 180);
}

void moveBackward()
{
  //RightSide move backward
  analogWrite(motorPin1, 0);
  analogWrite(motorPin2, 180);

  //LeftSide move backward
  analogWrite(motorPin3, 180);
  analogWrite(motorPin4, 0);
}

void stopMoving()
{
  analogWrite(motorPin1, 0);
  analogWrite(motorPin2, 0);

  analogWrite(motorPin3, 0);
  analogWrite(motorPin4, 0);
}

void turnRight()
{
  //RightSide move backward
  analogWrite(motorPin1, 0);
  analogWrite(motorPin2, 180);

  //LeftSide move forward
  analogWrite(motorPin3, 0);
  analogWrite(motorPin4, 180);
}

void turnLeft()
{
  //RightSide move forward
  analogWrite(motorPin1, 180);
  analogWrite(motorPin2, 0);

  //LeftSide move backward
  analogWrite(motorPin3, 180);
  analogWrite(motorPin4, 0);
}

int lookRight()
{
  myServo.write(50);
  delay(180);
  int rightDistance = readPing();
  myServo.write(125);
  return rightDistance;
  delay(500);
}

int lookLeft()
{
  myServo.write(170);
  delay(180);
  int leftDistance = readPing();
  myServo.write(125);
  return leftDistance;
  delay(500);
}

int readPing()
{
  delay(70);
  int cm = sonar.ping_cm();
  if (cm == 0)
    cm = 250;
  return cm;
}

void selfDriveMode()
{
  distance = readPing();
  int distanceL = 0;
  int distanceR = 0;

  if (distance < 30 && !hasLooked)
  {
    stopMoving();
    moveBackward();
    delay(300);
    stopMoving();

    distanceL = lookLeft();

    distanceR = lookRight();

    hasLooked = true;

    if (distanceL > distanceR)
    {
      turnLeft();
      delay(625);
      stopMoving();
    }
    else
    {
      turnRight();
      delay(625);
      stopMoving();
    }
  }

  if (distance > 30)
  {
    moveForward();
    hasLooked = false;
  }

}




















