package com.example.alex.carduino;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

public class DriveDevice extends AppCompatActivity {

    //SPP UUID
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    String address = null;
    BluetoothAdapter btAdapter = null;
    BluetoothSocket btSocket = null;
    private ProgressDialog progress;
    private boolean isConnected = false;
    private View.OnClickListener stopButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "STOP", Toast.LENGTH_SHORT).show();
            stopMoving();
        }
    };

    private View.OnClickListener upButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "FORWARD", Toast.LENGTH_SHORT).show();
            moveForward();
        }
    };

    private View.OnClickListener downButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "BACKWARD", Toast.LENGTH_SHORT).show();
            moveBackward();
        }
    };

    private View.OnClickListener leftButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "LEFT", Toast.LENGTH_SHORT).show();
            moveLeft();
        }
    };

    private View.OnClickListener rightButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "RIGHT", Toast.LENGTH_SHORT).show();
            moveRight();
        }
    };

    private View.OnClickListener selfDriveButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "SLOW", Toast.LENGTH_SHORT).show();
            selfDrive();
        }
    };

    private View.OnClickListener mediumSpeedButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "MEDIUM", Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener fastSpeedButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast.makeText(getApplicationContext(), "FAST", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_drive_device);

        Intent newInt = getIntent();
        address = newInt.getStringExtra(ConnectToDevice.EXTRA_ADDRESS);

        new ConnectBT().execute(); //Call the class to connect

        //Binding the buttons with the app buttons
        Button upButton = findViewById(R.id.button3);
        Button downButton = findViewById(R.id.button4);
        Button leftButton = findViewById(R.id.button5);
        Button rightButton = findViewById(R.id.button6);
        Button stopButton = findViewById(R.id.button7);
        Button selfDrive = findViewById(R.id.button8);
        Button mediumSpeedButton = findViewById(R.id.button9);
        Button fastSpeedButton = findViewById(R.id.button10);

        //Assigning the listeners to the buttons
        upButton.setOnClickListener(upButtonListener);
        downButton.setOnClickListener(downButtonListener);
        leftButton.setOnClickListener(leftButtonListener);
        rightButton.setOnClickListener(rightButtonListener);
        stopButton.setOnClickListener(stopButtonListener);
        selfDrive.setOnClickListener(selfDriveButtonListener);
        mediumSpeedButton.setOnClickListener(mediumSpeedButtonListener);
        fastSpeedButton.setOnClickListener(fastSpeedButtonListener);

    }

    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    private void stopMoving() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("5".getBytes());
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    private void moveForward() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("1".getBytes());
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    private void moveBackward() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("2".getBytes());
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    private void moveRight() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("3".getBytes());
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    private void moveLeft() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("4".getBytes());
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    private void selfDrive() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("6".getBytes());
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    private void disconnect() {
        if (btSocket != null) {
            try {
                btSocket.close();
            } catch (IOException e) {
                msg("Error in sending the command. Recheck connection.");
            }
        }
    }

    //Maybe should be static
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(DriveDevice.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try {
                if (btSocket == null || !isConnected) {
                    btAdapter = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = btAdapter.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            } catch (IOException e) {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            } else {
                msg("Connected.");
                isConnected = true;
            }
            progress.dismiss();
        }
    }
}
