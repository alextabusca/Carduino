package com.example.alex.carduino;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class ConnectToDevice extends AppCompatActivity {

    public static String EXTRA_ADDRESS = "device_address";
    ListView listOfPairedDevices;
    ArrayList list = new ArrayList();
    private BluetoothAdapter btAdapter;
    private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
            // Get the device MAC address, the last 17 chars in the View
            String info = ((TextView) view).getText().toString();
            String addess = info.substring(info.length() - 17);

            //Make an intent to start new activity
            Intent i = new Intent(ConnectToDevice.this, DriveDevice.class);

            //Change the activity
            i.putExtra(EXTRA_ADDRESS, addess);
            startActivity(i);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_to_device);

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        listOfPairedDevices = findViewById(R.id.pairedListView);
        registerForContextMenu(listOfPairedDevices);
    }

    public void pairedDevicesList(View v) {
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice bt : pairedDevices) {
                list.add(bt.getName() + "\n" + bt.getAddress()); //Get the name and the address of the device
            }
        } else {
            Toast.makeText(getApplicationContext(), "No paired devices found", Toast.LENGTH_SHORT).show();
        }


        Toast.makeText(getApplicationContext(), "Showing paired devices", Toast.LENGTH_SHORT).show();
        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listOfPairedDevices.setAdapter(adapter);
        listOfPairedDevices.setOnItemClickListener(myListClickListener);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu: this adds items to the action if it is present
        //getMenuInflater().inflate(R.menu.menu_device_list, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        //Handle action bar item clicks here. The action bar will automatically handle clickon the Home/Up, so long as you specify a parent activity in AndroidManifest.xml
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            return true;

        return super.onOptionsItemSelected(item);
    }
}
