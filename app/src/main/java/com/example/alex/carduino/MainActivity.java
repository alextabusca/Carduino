package com.example.alex.carduino;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 99;
    protected static String LOG_TAG;
    Switch btSwitch;
    BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btSwitch = findViewById(R.id.switch1);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isEnabled()) {
                //Bluetooth is enabled
                btSwitch.setChecked(true);
            } else {
                //Bluetooth is not enabled
                btSwitch.setChecked(false);
            }
        } else {
            Log.e(LOG_TAG, "This device does not have a bluetooth adapter");
        }


        btSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_ENABLE_BT); // 0 after the comma
                } else {
                    bluetoothAdapter.disable();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == 0) {
                // If the resultCode is 0, the user selected "No" when prompt to
                // allow the app to enable bluetooth.
                // You may want to display a dialog explaining what would happen if
                // the user doesn't enable bluetooth.
                Toast.makeText(this, "Warning! Bluetooth is not turned on!", Toast.LENGTH_LONG).show();

                //Set the switch to off state
                btSwitch.setChecked(false);

            } else
                Log.i(LOG_TAG, "User allowed bluetooth access!");
        }
    }

    /**
     * Called when the user presses the Connect To Device button.
     */
    public void connectToDevice(View view) {
        Intent intent = new Intent(this, ConnectToDevice.class);
        startActivity(intent);
    }

//    public void driveDevice(View view) {
//        Intent intent = new Intent(this, DriveDevice.class);
//        startActivity(intent);
//    }
}
